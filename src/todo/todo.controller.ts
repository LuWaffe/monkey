import { Controller, Get, Post, Put, Delete } from '@nestjs/common';
import { TodoService } from './todo.service';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Get()
  getMeth(): string {
    return this.todoService.getMeth();
  }

  @Post()
  postMeth(): string {
    return this.todoService.postMeth();
  }

  @Put()
  putMeth(): string {
    return this.todoService.putMeth();
  }

  @Delete()
  deleteMeth(): string {
    return this.todoService.deleteMeth();
  }
}
