import { Injectable } from '@nestjs/common';

@Injectable()
export class TodoService {
  getMeth(): string {
    return 'get meth';
  }
  postMeth(): string {
    return 'post meth';
  }
  putMeth(): string {
    return 'put meth';
  }
  deleteMeth(): string {
    return 'delete meth';
  }
}
